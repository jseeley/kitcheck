require "#{Dir.pwd}/dictionary"

dict = Dictionary.new
strings = dict.find_all_strings ARGV[0]
dict.display_words(strings)

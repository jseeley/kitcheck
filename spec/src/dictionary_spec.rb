require 'spec_helper'

require "#{Dir.pwd}/dictionary"

RSpec.describe Dictionary do
  it 'contains pockets, rockets, and sockets!' do
    dict = Dictionary.new
    strings = dict.find_all_strings '762-5387'
    expect(dict.found_words(strings)).to include('pockets')
    expect(dict.found_words(strings)).to include('rockets')
    expect(dict.found_words(strings)).to include('sockets')
  end

  it 'contains act, bat, and cat!' do
    dict = Dictionary.new
    strings = dict.find_all_strings '228'
    expect(dict.found_words(strings)).to include('act')
    expect(dict.found_words(strings)).to include('bat')
    expect(dict.found_words(strings)).to include('cat')
  end

  it "10 doesn't have any matches" do
    dict = Dictionary.new
    strings = dict.find_all_strings '10'
    expect(dict.found_words(strings)).to be_empty
  end

  it "string don't have matches" do
    dict = Dictionary.new
    strings = dict.find_all_strings 'some_letters'
    expect(dict.found_words(strings)).to be_empty
  end
end

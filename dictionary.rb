class Dictionary

  def self.get_lookup_table
    return [
      '2' => ['a','b','c'],
      '3' => ['d','e','f'],
      '4' => ['g','h','i'],
      '5' => ['j','k','l'],
      '6' => ['m','n','o'],
      '7' => ['p','q','r','s'],
      '8' => ['t','u','v'],
      '9' => ['w','x','y','z']
    ]
  end

  def find_all_strings(phone_number)
    allowed_chars = ['2','3','4','5','6','7','8','9']
    phone_number_arr = phone_number.split('').delete_if{ |x| !allowed_chars.include? x }
    lookup = (Dictionary.get_lookup_table)[0]

    chars_array = []
    phone_number_arr.each do |num|
      chars_array << lookup[num]
    end

    return [] if chars_array.empty?
    perms_array = chars_array.inject(&:product).map(&:flatten)

    strings = []
    perms_array.each do |val|
      strings << val.join
    end

    strings
  end

  def found_words(strings)
    found_words = []
    File.open("#{Dir.pwd}/sample_dictionary.txt").each do |word|
      sanitized_word = word.strip.downcase
      if strings.include? sanitized_word
        found_words << sanitized_word
      end
    end
    found_words
  end

  def display_words(strings)
    found_words(strings).each do |word|
      puts word
    end
  end
end

